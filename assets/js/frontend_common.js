$(document).ready(function(){

    // Kitaplarım sayfasında bulunan kitap filtreleme alanının gösterilmesi
    $(document).delegate('.btnToggleFilterSelections', 'click', function(){
       $("#book_filters_area").toggle("slow"); 
    });
    
    // Kitap Adetine Göre Fiyatlandırma
  
    $(document).delegate('.btnChangeCount', 'click', function(){
        var total_price = 0;
        var price = 39.99;  
        var count = $(".book_price").data('count');
        var value = $(this).data('value');
        switch(value)
        {
            case 0:
            if(count > 1)
            {
                count--;
            }
            break;
            case 1: count++; break;
        }
        total_price = parseFloat(price * count);
        $(".book_price").data("count", count);
        $(".book_price").val(total_price.toFixed(2)); 
        $(".lblCountInfo").html(count + ' Adet - ' + total_price.toFixed(2) + " ₺");
    });
    
    // Yorum yapma modelini açar
    $(document).delegate('.btnMakeComment', 'click', function(){
        $("#comment_modal").modal({
            show : true
        })
    });
    
});